package com.example.antonija.projectapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HelpActivity extends AppCompatActivity {

    @BindView(R.id.vpPager)
    ViewPager vpPager;
    @BindView(R.id.tlTabs)
    TabLayout tlTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        this.loadFragments();
    }
    private void loadFragments()
    { ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.insertFragment(new HSFragment(),HSFragment.TITLE);
        adapter.insertFragment(new CollegeFragment(),CollegeFragment.TITLE);
        this.vpPager.setAdapter(adapter);
        this.tlTabs.setupWithViewPager(this.vpPager);
    }
    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_without_test, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();}
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion

}
