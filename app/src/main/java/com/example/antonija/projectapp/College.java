package com.example.antonija.projectapp;

/**
 * Created by Antonija on 27/11/2017.
 */

public class College {
    private String _id;
    private String _study;
    private String _course;
    private String _field;
    private String _name;
    private String _location;
    private String _website;
    private String _category;
    private String _latlong;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_study() {
        return _study;
    }

    public void set_study(String _study) {
        this._study = _study;
    }

    public String get_course() {
        return _course;
    }

    public void set_course(String _course) {
        this._course = _course;
    }

    public String get_field() {
        return _field;
    }

    public void set_field(String _field) {
        this._field = _field;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

    public String get_website() {
        return _website;
    }

    public void set_website(String _website) {
        this._website = _website;
    }

    public String get_category() {
        return _category;
    }

    public void set_category(String _category) {
        this._category = _category;
    }

    public String get_latlong() {
        return _latlong;
    }

    public void set_latlong(String _latlong) {
        this._latlong = _latlong;
    }


}
