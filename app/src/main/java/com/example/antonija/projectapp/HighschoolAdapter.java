package com.example.antonija.projectapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 27/11/2017.
 */

public class HighschoolAdapter extends ArrayAdapter<Highschool>
{
    private Activity activity;
    int id;
    ArrayList<Highschool> highschools;
    public HighschoolAdapter(@NonNull Activity context, int resource, @NonNull ArrayList<Highschool> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.id = resource;
        this.highschools=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        HighSchoolHolder highschoolholder;

        if(convertView==null)
        {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView= inflater.inflate(id, null);
            highschoolholder = new HighSchoolHolder(convertView);
            convertView.setTag(highschoolholder);

        }
        else
        {
            highschoolholder = (HighSchoolHolder) convertView.getTag();
        }

        Highschool highschool = this.highschools.get(position);

        highschoolholder.tvHighSchoolId.setText(highschool.get_id());
        highschoolholder.tvHighSchoolName.setText(highschool.get_name());
        highschoolholder.tvHighSchoolType.setText(highschool.get_type());
        highschoolholder.tvHighSchoolProgram.setText(highschool.get_program());
        return convertView;
    }
    static class HighSchoolHolder
    {
        @BindView(R.id.tvHighSchoolId) TextView tvHighSchoolId;
        @BindView(R.id.tvHighSchoolName) TextView tvHighSchoolName;
        @BindView(R.id.tvHighSchoolType) TextView tvHighSchoolType;
        @BindView(R.id.tvHighSchoolProgram) TextView tvHighSchoolProgram;

        public HighSchoolHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }

}
