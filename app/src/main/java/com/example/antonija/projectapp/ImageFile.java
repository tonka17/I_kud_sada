package com.example.antonija.projectapp;


/**
 * Created by Antonija on 09/01/2018.
 */

public class ImageFile
{
    public String _id;
    public String _FileName;
    public String _UriPath;


    public ImageFile() {
    }

    public ImageFile(String _id, String _FileName, String _UriPath) {
        this._id = _id;
        this._FileName = _FileName;
        this._UriPath = _UriPath;

    }

   public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_UriPath() {
        return _UriPath;
    }

    public void set_UriPath(String _UriPath) {
        this._UriPath = _UriPath;
    }

    public String get_FileName() {
        return _FileName;
    }

    public void set_FileName(String _FileName) {
        this._FileName = _FileName;
    }


}
