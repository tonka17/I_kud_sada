package com.example.antonija.projectapp;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ivHighSchoolTitle)
    ImageView ivHighSchoolTitle;
    @BindView(R.id.ivCollegeTitle)
    ImageView ivCollegeTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadPictures();

    }

    private void loadPictures() {
        Picasso.with(getApplicationContext()).load(R.drawable.highschool).fit().centerCrop().into(ivHighSchoolTitle);
        Picasso.with(getApplicationContext()).load(R.drawable.college).fit().centerCrop().into(ivCollegeTitle);
    }
    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();
        }
        return false;
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion

    @OnClick(R.id.ivHighSchoolTitle)
    public void goToHighSchoolActivity()
    {
        Intent intent = new Intent();
        intent.setClass(this, HighSchoolListActivity.class);
        this.startActivity(intent);
    }

    @OnClick(R.id.ivCollegeTitle)
    public void goToCollegeActivity()
    {
        Intent intent = new Intent();
        intent.setClass(this, CollegeListActivity.class);
        this.startActivity(intent);
    }
}
