package com.example.antonija.projectapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 09/01/2018.
 */

public class ImageFileAdapter extends ArrayAdapter<ImageFile>
    {

    ArrayList<ImageFile> _ImageFiles;
    private Context _context;
    private int layoutResourceId;

    public ImageFileAdapter(Context _context, int layoutResourceId, ArrayList<ImageFile> images) {
        super(_context, layoutResourceId, images);
        this.layoutResourceId = layoutResourceId;
        this._context = _context;
        this._ImageFiles = images;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageFileHolder imageHolder;
        if (convertView==null)
        {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_file_item, parent, false);
            imageHolder = new ImageFileHolder(convertView);
            convertView.setTag(imageHolder);
        }
        else
        {
            imageHolder = (ImageFileHolder) convertView.getTag();

        }
        ImageFile imagefile = this._ImageFiles.get(position);
        imageHolder.tvImageName.setText(imagefile.get_FileName());

        Picasso.with(parent.getContext()).load(imagefile.get_UriPath()).fit().centerCrop().into(imageHolder.ivThumbnail);

        return convertView;
    }
    static class ImageFileHolder
    {
        @BindView(R.id.ivThumbnail)
        ImageView ivThumbnail;
        @BindView (R.id.tvImageName)
        TextView tvImageName;



        public ImageFileHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }

}

