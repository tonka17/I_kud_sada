package com.example.antonija.projectapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsCollegeActivity extends AppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.tvDetailsNameText)
    TextView tvDetailsNameText;
    @BindView(R.id.tvDetailsStudyText)
    TextView tvDetailsStudyText;
    @BindView(R.id.tvDetailsCourseText)
    TextView tvDetailsCourseText;
    @BindView(R.id.tvDetailsCategoryText)
    TextView tvDetailsCategoryText;
    @BindView(R.id.tvDetailsFieldText)
    TextView tvDetailsFieldText;
    @BindView(R.id.tvDetailsLocationText)
    TextView tvDetailsLocationText;
    @BindView(R.id.tvDetailsWebsiteText)
    TextView tvDetailsWebsiteText;
    @BindView(R.id.main_scrollview)
    ScrollView main_scrollview;
    @BindView(R.id.transparent_image)
    ImageView transparent_image;

    public static final String KEY_NAME = "name";
    public static final String KEY_STUDY = "study";
    public static final String KEY_COURSE = "course";
    public static final String KEY_FIELD = "field";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_WEBSITE = "website";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_LATLONG = "latlong";

    String latlong = "";
    String name = "";
    double lat = 0;
    double lng = 0;

    GoogleMap _GoogleMap;
    SupportMapFragment _MapFragment;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_college);
        ButterKnife.bind(this);
        this.handleStartingIntent(this.getIntent());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        transparent_image.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    private void handleStartingIntent(Intent intent)
    {
        if (intent != null) {
            if (intent.hasExtra(KEY_NAME)) {
                name = intent.getStringExtra(KEY_NAME);
                tvDetailsNameText.setText(name);
            }
            if(intent.hasExtra(KEY_STUDY))
            {
                String study = intent.getStringExtra(KEY_STUDY);
                tvDetailsStudyText.setText(study);
            }
            if(intent.hasExtra(KEY_COURSE))
            {
                String course = intent.getStringExtra(KEY_COURSE);
                tvDetailsCourseText.setText(course);
            }
            if(intent.hasExtra(KEY_CATEGORY))
            {
                String category = intent.getStringExtra(KEY_CATEGORY);
                tvDetailsCategoryText.setText(category);
            }
            if(intent.hasExtra(KEY_FIELD))
            {
                String field = intent.getStringExtra(KEY_FIELD);
                tvDetailsFieldText.setText(field);
            }
            if(intent.hasExtra(KEY_LOCATION))
            {
                String location = intent.getStringExtra(KEY_LOCATION);
                tvDetailsLocationText.setText(location);
            }
            if(intent.hasExtra(KEY_WEBSITE))
            {
                String website = intent.getStringExtra(KEY_WEBSITE);
                tvDetailsWebsiteText.setText(website);
            }
            if(intent.hasExtra(KEY_LATLONG))
            {
                latlong = intent.getStringExtra(KEY_LATLONG);
                _MapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fGoogleMap);
                _MapFragment.getMapAsync(this);

                String [] coordinates = latlong.split(",");
                String latitude = coordinates[0];
                String longitude = coordinates[1];
                lat = Double.parseDouble(latitude);
                lng = Double.parseDouble(longitude);
            }

        }
    }
    //region openWebsite
    @OnClick(R.id.tvDetailsWebsiteText)
    public void openLink()
    {
        String address = tvDetailsWebsiteText.getText().toString();
        if(address.startsWith("http://")==false)
        {
            address="http://" + address;
        }
        Uri data = Uri.parse(address);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(data);
        if(canBeCalled(intent))
        {
            startActivity(intent);
        }
    }
    private boolean canBeCalled(Intent intent)
    {
        PackageManager pm = this.getPackageManager();
        if(intent.resolveActivity(pm)==null)
        {
            return  false;
        }
        else
        {
            return true;
        }
    }
    //endregion


    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_test:
                goToTest();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();
        }
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToTest() {
        Intent intent = new Intent();
        intent.setClass(this, TestCollegeActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion
    public void onMapReady(GoogleMap googleMap) {
        this._GoogleMap = googleMap;
        UiSettings uiSettings = this._GoogleMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        LatLng coordinates = new LatLng(lat, lng);

        CameraPosition cameraPosition = CameraPosition.builder()
                .zoom(17).tilt(20).target(coordinates).build();
        _GoogleMap.addMarker(new MarkerOptions().position(coordinates)
                .title(name));
        _GoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling  ActivityCompat#requestPermissions
            // See the last example on how to do this.
            return;
        }
        this._GoogleMap.setMyLocationEnabled(true);

    }
}