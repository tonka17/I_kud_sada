package com.example.antonija.projectapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class CollegeListActivity extends AppCompatActivity {

    @BindView(R.id.bCollegeChoice)
    ImageButton bCollegeChoice;
    @BindView(R.id.lvCollegeList)
    ListView lvCollegeList;

    private DataBaseHelper dbHelper;
    CollegeAdapter collegeAdapter;
    ArrayList<College> collegeArrayList = new ArrayList<College>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_college_list);
        ButterKnife.bind(this);
        Picasso.with(getApplicationContext()).load(R.drawable.searchiconwhite32px).fit().centerInside().into(bCollegeChoice);
        this.loadDatabase();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_test:
                goToTest();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();}
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToTest() {
        Intent intent = new Intent();
        intent.setClass(this, TestCollegeActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion

    private void loadDatabase()
    {
        dbHelper=new DataBaseHelper(this);
        try
        {
            dbHelper.checkAndCopyDatabase();
            dbHelper.openDatabase();
        }
        catch (SQLException e)
        {
        }
        try
        {
            Cursor cursor = dbHelper.QueryData("select * from CollegeTable");
            if (cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do
                    {
                        College college = new College();
                        college.set_id(cursor.getString(0));
                        college.set_study(cursor.getString(1));
                        college.set_course(cursor.getString(2));
                        college.set_field(cursor.getString(3));
                        college.set_name(cursor.getString(4));
                        college.set_location(cursor.getString(5));
                        college.set_website(cursor.getString(6));
                        college.set_category(cursor.getString(7));
                        college.set_latlong(cursor.getString(8));
                        collegeArrayList.add(college);
                    }
                    while (cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
        }
        collegeAdapter = new CollegeAdapter(this, R.layout.college_list_item, collegeArrayList);
        lvCollegeList.setAdapter(collegeAdapter);
        collegeAdapter.notifyDataSetChanged();
    }


    @OnClick(R.id.bCollegeChoice)
    public void goToSearch()
    {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SearchCollegeActivity.class);
        this.startActivity(intent);
    }
    @OnItemClick(R.id.lvCollegeList)
    public void showDetails(int position)
    {
        College selectedCollege = (College) lvCollegeList.getItemAtPosition(position);
        String name = selectedCollege.get_name();
        String study = selectedCollege.get_study();
        String course = selectedCollege.get_course();
        String category = selectedCollege.get_category();
        String field = selectedCollege.get_field();
        String location = selectedCollege.get_location();
        String website = selectedCollege.get_website();
        String latlong = selectedCollege.get_latlong();
        Intent intent = new Intent();
        intent.setClass(this, DetailsCollegeActivity.class);
        intent.putExtra(DetailsCollegeActivity.KEY_NAME, name);
        intent.putExtra(DetailsCollegeActivity.KEY_STUDY, study);
        intent.putExtra(DetailsCollegeActivity.KEY_COURSE, course);
        intent.putExtra(DetailsCollegeActivity.KEY_CATEGORY, category);
        intent.putExtra(DetailsCollegeActivity.KEY_FIELD, field);
        intent.putExtra(DetailsCollegeActivity.KEY_LOCATION, location);
        intent.putExtra(DetailsCollegeActivity.KEY_WEBSITE, website);
        intent.putExtra(DetailsCollegeActivity.KEY_LATLONG, latlong);
        this.startActivity(intent);
    }
}