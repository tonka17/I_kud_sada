package com.example.antonija.projectapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonija on 27/11/2017.
 */

public class CollegeAdapter extends ArrayAdapter<College> {

    private Activity activity;
    int id;
    ArrayList<College> colleges;

    public CollegeAdapter(@NonNull Activity context, int resource, @NonNull ArrayList<College> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.id = resource;
        this.colleges = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CollegeHolder collegeHolder;

        if(convertView==null)
        {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView= inflater.inflate(id, null);
            collegeHolder = new CollegeHolder(convertView);
            convertView.setTag(collegeHolder);
        }
        else
        {
            collegeHolder = (CollegeHolder) convertView.getTag();
        }
        College college = colleges.get(position);

        collegeHolder.tvCollegeId.setText(college.get_id());
        collegeHolder.tvCollegeStudy.setText(college.get_study());
        collegeHolder.tvCollegeCourse.setText(college.get_course());
        collegeHolder.tvCollegeField.setText(college.get_field());
        collegeHolder.tvCollegeName.setText(college.get_name());

        return convertView;
    }
    static class CollegeHolder
    {
        @BindView((R.id.tvCollegeId)) TextView tvCollegeId;
        @BindView(R.id.tvCollegeStudy) TextView tvCollegeStudy;
        @BindView(R.id.tvCollegeCourse) TextView tvCollegeCourse;
        @BindView(R.id.tvCollegeField) TextView tvCollegeField;
        @BindView(R.id.tvCollegeName) TextView tvCollegeName;

        public CollegeHolder(View view)
        {
            ButterKnife.bind(this, view);
        }
    }

}
