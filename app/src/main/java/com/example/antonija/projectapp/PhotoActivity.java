package com.example.antonija.projectapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoActivity extends AppCompatActivity {


    public  static final String KEY_URL = "url";
    String url = "";

    @BindView(R.id.ivPhoto)
    ImageView ivPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        this.handleStartingIntent(this.getIntent());
    }

    private void handleStartingIntent(Intent intent) {
        if (intent != null) {
            if (intent.hasExtra(KEY_URL)) {
                url = intent.getStringExtra(KEY_URL);
                Picasso.with(this).load(url).centerInside().resize(300,600).into(ivPhoto);
            }
        }
    }


}
