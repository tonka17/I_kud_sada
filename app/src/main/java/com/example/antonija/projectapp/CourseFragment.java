package com.example.antonija.projectapp;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by Student on 2.10.2017..
 */

public class CourseFragment extends Fragment
{
    @BindView(R.id.etEnterCollegeCourse)
    EditText etEnterCollegeCourse;
    @BindView(R.id.bCollegeCourse)
    Button bCollegeCourse;
    @BindView(R.id.lvCollegeList)
    ListView lvCollegeList;

    public static final String TITLE = "Smjer";

    private DataBaseHelper dbHelper;
    CollegeAdapter collegeAdapter;
    ArrayList<College> collegeArrayList = new ArrayList<College>();

    public CourseFragment()
    {

    }
    @Override public void onCreate
            (@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }
    @Override public View onCreateView
            (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_course, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @OnClick(R.id.bCollegeCourse)
    public void filterCourse()
        {
            String entry = etEnterCollegeCourse.getText().toString();
            collegeArrayList.clear();
            dbHelper=new DataBaseHelper(getContext());
            try
            {
                dbHelper.checkAndCopyDatabase();
                dbHelper.openDatabase();
            }
            catch (SQLException e)
            {
            }
            try
            {
                Cursor cursor = dbHelper.QueryData("select * from CollegeTable where lower(course) like '" + entry + "%'");
                if (cursor!=null)
                {
                    if(cursor.moveToFirst())
                    {
                        do
                        {
                            College college = new College();
                            college.set_id(cursor.getString(0));
                            college.set_study(cursor.getString(1));
                            college.set_course(cursor.getString(2));
                            college.set_field(cursor.getString(3));
                            college.set_name(cursor.getString(4));
                            college.set_location(cursor.getString(5));
                            college.set_website(cursor.getString(6));
                            college.set_category(cursor.getString(7));
                            college.set_latlong(cursor.getString(8));
                            collegeArrayList.add(college);
                        }
                        while (cursor.moveToNext());
                    }
                }
            }
            catch (SQLException e)
            {
            }
            collegeAdapter = new CollegeAdapter((Activity) getContext(), R.layout.college_list_item, collegeArrayList);
            lvCollegeList.setAdapter(collegeAdapter);
            collegeAdapter.notifyDataSetChanged();
        }
    @OnItemClick(R.id.lvCollegeList)
    public void showDetails(int position)
    {
        College selectedCollege = (College) lvCollegeList.getItemAtPosition(position);
        String name = selectedCollege.get_name();
        String study = selectedCollege.get_study();
        String course = selectedCollege.get_course();
        String category = selectedCollege.get_category();
        String field = selectedCollege.get_field();
        String location = selectedCollege.get_location();
        String website = selectedCollege.get_website();
        String latlong = selectedCollege.get_latlong();
        Intent intent = new Intent();
        intent.setClass(getContext(), DetailsCollegeActivity.class);
        intent.putExtra(DetailsCollegeActivity.KEY_NAME, name);
        intent.putExtra(DetailsCollegeActivity.KEY_STUDY, study);
        intent.putExtra(DetailsCollegeActivity.KEY_COURSE, course);
        intent.putExtra(DetailsCollegeActivity.KEY_CATEGORY, category);
        intent.putExtra(DetailsCollegeActivity.KEY_FIELD, field);
        intent.putExtra(DetailsCollegeActivity.KEY_LOCATION, location);
        intent.putExtra(DetailsCollegeActivity.KEY_WEBSITE, website);
        intent.putExtra(DetailsCollegeActivity.KEY_LATLONG, latlong);
        this.startActivity(intent);
    }
}
