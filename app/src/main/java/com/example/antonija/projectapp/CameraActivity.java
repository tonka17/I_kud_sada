package com.example.antonija.projectapp;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class CameraActivity extends AppCompatActivity implements ValueEventListener {

    @BindView(R.id.bCapture)
    Button bCapture;
    @BindView(R.id.gvImages)
    GridView gvImages;
    @BindView(R.id.pbUploadProgress)
    ProgressBar pbUploadProgress;

    ImageFileAdapter imageFileAdapter;
    ArrayList<ImageFile> imageGridData;

    private static final String IMAGEFILES = "images" ;
    private static final String KEY_FILE_PATH = "filepath";
    public static final int CAMERA_REQUEST_CODE = 1;

    public String mFilePath;
    public String _photoURL;

    int progress = 0;

    public StorageReference storageRef;
    public StorageReference pathRef;

    public DatabaseReference _Database;

    public Uri downloadUrl;
    public Uri pictureUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this._Database = FirebaseDatabase.getInstance().getReference(IMAGEFILES);
        this._Database.addValueEventListener(this);

        storageRef = FirebaseStorage.getInstance().getReference(IMAGEFILES);


        imageGridData = new ArrayList<>();
        imageFileAdapter = new ImageFileAdapter(this, R.layout.image_file_item, imageGridData);
        gvImages.setAdapter(imageFileAdapter);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_FILE_PATH)) {
                this.mFilePath = savedInstanceState.getString(KEY_FILE_PATH);
            }
        }
    }

    @OnItemClick(R.id.gvImages)
    public void showPhoto(int position)
    {
        ImageFile photo = (ImageFile) gvImages.getItemAtPosition(position);
        String url = photo.get_UriPath();
        Intent intent = new Intent();
        intent.setClass(this, PhotoActivity.class);
        intent.putExtra(PhotoActivity.KEY_URL, url);
        this.startActivity(intent);
    }

    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_without_test, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();
        }
        return false;
    }


    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }


    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }

    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
//endregion

    //region TakePhoto
    @OnClick(R.id.bCapture)

    public void takePicture(){
        Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePic.resolveActivity(getPackageManager()) != null){
            startActivityForResult(takePic, CAMERA_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            pictureUri = data.getData();
            this.mFilePath = getPathFromUri(pictureUri);
            this.uploadImage();
        }
    }
    private String getPathFromUri(Uri pictureUri) {
        Cursor cursor  = null;
        String data = MediaStore.Images.Media.DATA;

        String[] projection = new String[]{data};
        cursor = this.getContentResolver().query(pictureUri,projection,null,null,null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(data));
        return path;
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mFilePath != null) {
            outState.putString(KEY_FILE_PATH, this.mFilePath);
        }
    }

//endregion

    //region uploadImage
    private void uploadImage()
    {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String imageFileName = timeStamp + "_";
        setProgressValue(progress);

        if(pictureUri != null)
        {
            pathRef = storageRef.child(imageFileName);
            pathRef.putFile(pictureUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        downloadUrl = taskSnapshot.getDownloadUrl();
                            String id = _Database.push().getKey();
                        _photoURL = String.valueOf(downloadUrl);
                        ImageFile imagefile = new ImageFile(id, imageFileName, _photoURL);
                            _Database.child(imagefile.get_id()).setValue(imagefile);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        }
                    });
        }

    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        ArrayList<ImageFile> imageFiles = new ArrayList<>();
        for (DataSnapshot ds : dataSnapshot.getChildren())
        {
            ImageFile imagefile=ds.getValue(ImageFile.class);

            imageFiles.add(imagefile);
        }

        imageFileAdapter = new ImageFileAdapter(CameraActivity.this, R.layout.image_file_item, imageFiles);
        gvImages.setAdapter(imageFileAdapter);
        setProgressValue(progress);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
    //endregion

    private void setProgressValue(final int progress) {

        // set the progress
        pbUploadProgress.setProgress(progress);
        // thread is used to change the progress value
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setProgressValue(progress + 10);
            }
        });
        thread.start();
    }
}
