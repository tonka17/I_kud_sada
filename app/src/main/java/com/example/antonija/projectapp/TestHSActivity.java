package com.example.antonija.projectapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestHSActivity extends AppCompatActivity {


    @BindView(R.id.bShowResult)
    Button bShowResult;

    ArrayList<String> natural = new ArrayList<String>();
    ArrayList<String> linguistic = new ArrayList<String>();
    ArrayList<String> social = new ArrayList<String>();
    ArrayList<String> technical = new ArrayList<String>();
    ArrayList<String> artistic = new ArrayList<String>();
    ArrayList<String> physical = new ArrayList<String>();
    ArrayList<String> health = new ArrayList<String>();
    ArrayList<String> agriculture = new ArrayList<String>();
    ArrayList<String> craft = new ArrayList<String>();
    ArrayList<String> service = new ArrayList<String>();
    ArrayList<String> construction = new ArrayList<String>();
    ArrayList<String> commerce = new ArrayList<String>();
    ArrayList<String> administration = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_hs);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    //region Checkboxes
    public void selectItem(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        switch(view.getId()) {
            case R.id.cbHJ:
                if (checked) {
                    linguistic.add("HJ");
                    social.add("HJ");
                }
                else
                {
                    linguistic.remove("HJ");
                    social.remove("HJ");
                }
                break;
            case R.id.cbMat:
                if (checked) {
                    natural.add("Mat");
                }
                else
                {
                    natural.remove("Mat");
                }
                break;
            case R.id.cbEJ:
                if (checked) {
                    linguistic.add("EJ");
                    social.add("EJ");
                }
                else
                {
                    linguistic.remove("EJ");
                    social.remove("EJ");
                }
                break;
            case R.id.cbBio:
                if (checked)
                {
                    natural.add("Bio");
                }
                else
                {
                    natural.remove("Bio");
                }
                break;
            case R.id.cbKem:
                if (checked)
                {
                    natural.add("Kem");
                }
                else
                {
                    natural.remove("Kem");
                }
                break;
            case R.id.cbFiz:
                if (checked)
                {
                    natural.add("Fiz");
                }
                else
                {
                    natural.remove("Fiz");
                }
                break;
            case R.id.cbInfo:
                if (checked)
                {
                    natural.add("Info");
                    technical.add("Info");
                }
                else
                {
                    natural.remove("Info");
                    technical.remove("Info");
                }
                break;
            case R.id.cbPov:
                if (checked)
                {
                    social.add("Pov");
                }
                else
                {
                    social.remove("Pov");
                }
                break;
            case R.id.cbGeo:
                if (checked)
                {
                    natural.add("Geo");
                }
                else
                {
                    natural.remove("Geo");
                }
                break;
            case R.id.cbTeh:
                if (checked)
                {
                    technical.add("Teh");
                }
                else
                {
                    technical.remove("Teh");
                }
                break;
            case R.id.cbTZK:
                if (checked)
                {
                    physical.add("TZK");
                }
                else
                {
                    physical.remove("TZK");
                }
                break;
            case R.id.cbLik:
                if (checked)
                {
                    artistic.add("Lik");
                }
                else
                {
                    artistic.remove("Lik");
                }
                break;
            case R.id.cbGlazb:
                if (checked)
                {
                    artistic.add("Glazb");
                }
                else
                {
                    artistic.remove("Glazb");
                }
                break;
            case R.id.cbVjer:
                if (checked)
                {
                    social.add("Vjer");
                }
                else
                {
                    social.remove("Vjer");
                }
                break;
            case R.id.cbDraw:
                if (checked)
                {
                    artistic.add("Draw");
                }
                else
                {
                    artistic.remove("Draw");
                }
                break;
            case R.id.cbWrite:
                if (checked)
                {
                    artistic.add("Write");
                }
                else
                {
                    artistic.remove("Write");
                }
                break;
            case R.id.cbHair:
                if (checked)
                {
                    craft.add("Hair");
                    service.add("Hair");
                }
                else
                {
                    craft.remove("Hair");
                    service.remove("Hair");
                }
                break;
            case R.id.cbRead:
                if (checked)
                {
                    linguistic.add("Read");
                    administration.add("Read");
                }
                else
                {
                    linguistic.remove("Read");
                    administration.remove("Read");
                }
                break;
            case R.id.cbPlant:
                if (checked)
                {
                    natural.add("Plant");
                    agriculture.add("Plant");
                }
                else
                {
                    natural.remove("Plant");
                    agriculture.remove("Plant");
                }
                break;
            case R.id.cbSew:
                if (checked)
                {
                    craft.add("Sew");
                    artistic.add("Sew");
                }
                else
                {
                    craft.remove("Sew");
                    artistic.remove("Sew");
                }
                break;
            case R.id.cbCook:
                if (checked)
                {
                    craft.add("Cook");
                    service.add("Cook");
                }
                else
                {
                    craft.remove("Cook");
                    service.remove("Cook");
                }
                break;
            case R.id.cbHelp:
                if (checked)
                {
                    health.add("Help");
                    social.add("Help");
                    natural.add("Help");
                }
                else
                {
                    health.remove("Help");
                    social.remove("Help");
                    natural.remove("Help");
                }
                break;
            case R.id.cbSport:
                if (checked)
                {
                    physical.add("Sport");
                }
                else
                {
                    physical.remove("Sport");
                }
                break;
            case R.id.cbCar:
                if (checked)
                {
                    technical.add("Car");
                    craft.add("Car");
                }
                else
                {
                    technical.remove("Car");
                    craft.remove("Car");
                }
                break;
            case R.id.cbSell:
                if (checked)
                {
                    commerce.add("Sell");
                }
                else
                {
                    commerce.remove("Sell");
                }
                break;
            case R.id.cbComputer:
                if (checked)
                {
                    technical.add("Computer");
                }
                else
                {
                    technical.remove("Computer");
                }
                break;
            case R.id.cbCalc:
                if (checked)
                {
                    technical.add("Calc");
                    administration.add("Calc");
                    natural.add("Calc");
                }
                else
                {
                    technical.remove("Calc");
                    administration.remove("Calc");
                    natural.remove("Calc");
                }
                break;
            case R.id.cbLanguage:
                if (checked)
                {
                    social.add("Language");
                    linguistic.add("Language");
                }
                else
                {
                    social.remove("Language");
                    linguistic.remove("Language");
                }
                break;
            case R.id.cbLawyer:
                if (checked)
                {
                    administration.add("Lawyer");
                    social.add("Lawyer");
                }
                else
                {
                    administration.remove("Lawyer");
                    social.remove("Lawyer");
                }
                break;
            case R.id.cbMusic:
                if (checked)
                {
                    artistic.add("Music");
                }
                else
                {
                    artistic.remove("Music");
                }
                break;
            case R.id.cbReport:
                if (checked)
                {
                    administration.add("Report");
                }
                else
                {
                    administration.remove("Report");
                }
                break;
            case R.id.cbHotel:
                if (checked)
                {
                    service.add("Hotel");
                }
                else
                {
                    service.remove("Hotel");
                }
                break;
            case R.id.cbAnimal:
                if (checked)
                {
                    natural.add("Animal");
                }
                else
                {
                    natural.remove("Animal");
                }
                break;
            case R.id.cbBuild:
                if (checked)
                {
                    construction.add("Build");
                }
                else
                {
                    construction.remove("Build");
                }
                break;
            case R.id.cbOffice:
                if (checked)
                {
                    administration.add("Office");
                    technical.add("Office");
                    health.add("Office");
                }
                else
                {
                    administration.remove("Office");
                    technical.remove("Office");
                    health.remove("Office");
                }
                break;
            case R.id.cbOutside:
                if (checked)
                {
                    natural.add("Outside");
                    physical.add("Outside");
                    agriculture.add("Outside");
                    construction.add("Outside");
                }
                else
                {
                    natural.remove("Outside");
                    physical.remove("Outside");
                    agriculture.remove("Outside");
                    construction.remove("Outside");
                }
                break;
            case R.id.cbFactory:
                if (checked)
                {
                    construction.add("Factory");
                    craft.add("Factory");
                }
                else
                {
                    construction.remove("Factory");
                    craft.remove("Factory");
                }
                break;
            case R.id.cbPeople:
                if (checked)
                {
                    artistic.add("People");
                    physical.add("People");
                    health.add("People");
                    linguistic.add("People");
                    social.add("People");
                    commerce.add("People");
                    service.add("People");
                }
                else
                {
                    artistic.remove("People");
                    physical.remove("People");
                    health.remove("People");
                    linguistic.remove("People");
                    social.remove("People");
                    commerce.remove("People");
                    service.remove("People");
                }
                break;
            case R.id.cbAlone:
                if (checked)
                {
                    natural.add("Alone");
                    agriculture.add("Alone");
                    technical.add("Alone");
                    craft.add("Alone");
                    artistic.add("Alone");
                }
                else
                {
                    natural.remove("Alone");
                    agriculture.remove("Alone");
                    technical.remove("Alone");
                    craft.remove("Alone");
                    artistic.remove("Alone");
                }
                break;

        }
    }
    //endregion

    //region Results
    @OnClick(R.id.bShowResult)
    public void setbShowResult()
    {
        int result = calculateInterest();
        String profession = "profession";
        if(natural.size()==result)
            profession = "natural";
        if(social.size()==result)
            profession = "social";
        if(technical.size()==result)
            profession = "technical";
        if(artistic.size()==result)
            profession = "artistic";
        if(health.size()==result)
            profession = "health";
        if(linguistic.size()==result)
            profession = "linguistic";
        if(agriculture.size()==result)
            profession = "agriculture";
        if(construction.size()==result)
            profession = "construction";
        if(administration.size()==result)
            profession = "administration";
        if(craft.size()==result)
            profession = "craft";
        if(service.size()==result)
            profession = "service";
        if(commerce.size()==result)
            profession = "commerce";
        if(physical.size()==result)
            profession = "physical";
        Intent intent = new Intent();
        intent.setClass(this, HSTestResultActivity.class);
        intent.putExtra(HSTestResultActivity.KEY_PROFESSION, profession);
        this.startActivity(intent);
    }
    public int calculateInterest()
        {
            int [] professions = new int[] {natural.size(), social.size(), linguistic.size(), technical.size(), artistic.size(), health.size(), agriculture.size(), craft.size(), service.size(), commerce.size(), administration.size(), physical.size(), construction.size()};
            int larger;
            for(int i = 0; i<professions.length; i++)
            {
                larger = 0;
                for(int j = i+1; j<professions.length; j++)
                {
                    if(professions[i]<professions[j])
                    {
                        larger = professions[i];
                        professions[i] = professions[j];
                        professions[j] = larger;
                    }
                }
            }
            return professions[0];
        }

        //endregion
    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_without_test, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();}
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion
}
