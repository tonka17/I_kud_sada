package com.example.antonija.projectapp;

/**
 * Created by Antonija on 27/11/2017.
 */

public class Highschool {
    private String _id;
    private String _name;
    private String _type;
    private String _program;
    private String _location;
    private String _website;
    private String _latlong;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public String get_program() {
        return _program;
    }

    public void set_program(String _program) {
        this._program = _program;
    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

    public String get_website() {
        return _website;
    }

    public void set_website(String _website) {
        this._website = _website;
    }

    public String get_latlong() {
        return _latlong;
    }

    public void set_latlong(String _latlong) {
        this._latlong = _latlong;
    }
}
