package com.example.antonija.projectapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestCollegeActivity extends AppCompatActivity {

    @BindView(R.id.bShowResult)
    Button bShowResult;

    ArrayList<String> naturalScience = new ArrayList<String>();
    ArrayList<String> socialScience = new ArrayList<String>();
    ArrayList<String> technicalScience = new ArrayList<String>();
    ArrayList<String> artisticField = new ArrayList<String>();
    ArrayList<String> humanities = new ArrayList<String>();
    ArrayList<String> biotechnicalScience = new ArrayList<String>();
    ArrayList<String> healthMedicine = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_college);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_without_test, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();
        }
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }

    public void goToCamera() {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion

//region Checkboxes
    public void selectItem(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.cbSpeech:
                if (checked) {
                    humanities.add("Speech");
                    socialScience.add("Speech");
                    artisticField.add("Speech");
                } else {
                    humanities.remove("Speech");
                    socialScience.remove("Speech");
                    artisticField.remove("Speech");
                }
                break;
            case R.id.cbReading:
                if (checked) {
                    socialScience.add("Reading");
                    humanities.add("Reading");
                }
                else
                {
                    socialScience.remove("Reading");
                    humanities.remove("Reading");
                }
                break;
            case R.id.cbTeaching:
                if (checked) {
                    humanities.add("Teaching");
                }
                else
                {
                    humanities.remove("Teaching");
                }
                break;
            case R.id.cbHelping:
                if (checked) {
                    humanities.add("Helping");
                    healthMedicine.add("Helping");
                }
                else
                {
                    humanities.remove("Helping");
                    healthMedicine.remove("Helping");
                }
                break;
            case R.id.cbCalculating:
                if (checked) {
                    naturalScience.add("Calculating");
                    technicalScience.add("Calculating");
                }
                else
                {
                    naturalScience.remove("Calculating");
                    technicalScience.remove("Calculating");
                }
                break;
            case R.id.cbExperiments:
                if (checked) {
                    naturalScience.add("Experiments");
                }
                else
                {
                    naturalScience.remove("Experiments");
                }
                break;
            case R.id.cbAdministration:
                if (checked) {
                    socialScience.add("dministration");
                }
                else
                {
                    socialScience.remove("dministration");
                }
                break;
            case R.id.cbMusicAct:
                if (checked) {
                    artisticField.add("MusicAct");
                }
                else
                {
                    artisticField.remove("MusicAct");
                }
                break;
            case R.id.cbBuilding:
                if (checked) {
                    technicalScience.add("Building");
                }
                else
                {
                    technicalScience.remove("Building");
                }
                break;
            case R.id.cbProgramming:
                if (checked) {
                    technicalScience.add("Programming");
                }
                else
                {
                    technicalScience.remove("Programming");
                }
                break;
            case R.id.cbMechanical:
                if (checked) {
                    biotechnicalScience.add("Mechanical");
                }
                else
                {
                    biotechnicalScience.remove("Mechanical");
                }
                break;
            case R.id.cbPlantsAnimals:
                if (checked) {
                    biotechnicalScience.add("PlantsAnimals");
                }
                else
                {
                    biotechnicalScience.remove("PlantsAnimals");
                }
                break;
            case R.id.cbOrganized:
                if (checked) {
                    technicalScience.add("Organized");
                }
                else
                {
                    technicalScience.remove("Organized");
                }
                break;
            case R.id.cbCreativity:
                if (checked) {
                    artisticField.add("Creativity");
                }
                else
                {
                    artisticField.remove("Creativity");
                }
                break;
            case R.id.cbPrecision:
                if (checked) {
                    naturalScience.add("Precision");
                    healthMedicine.add("Precision");
                }
                else
                {
                    naturalScience.remove("Precision");
                    healthMedicine.remove("Precision");
                }
                break;
            case R.id.cbCommunications:
                if (checked) {
                    humanities.add("Communications");
                    socialScience.add("Communications");
                }
                else
                {
                    humanities.remove("Communications");
                    socialScience.remove("Communications");
                }
                break;
            case R.id.cbResearch:
                if (checked) {
                    naturalScience.add("Research");
                }
                else
                {
                    naturalScience.remove("Research");
                }
                break;
            case R.id.cbDexterity:
                if (checked) {
                    healthMedicine.add("Dexterity");
                    biotechnicalScience.add("Dexterity");
                }
                else
                {
                    healthMedicine.remove("Dexterity");
                    biotechnicalScience.remove("Dexterity");
                }
                break;
            case R.id.cbPerseverance:
                if (checked) {
                    technicalScience.add("Perseverance");
                    healthMedicine.add("Perseverance");
                }
                else
                {
                    technicalScience.remove("Perseverance");
                    healthMedicine.remove("Perseverance");
                }
                break;
            case R.id.cbClosedSpace:
                if (checked) {
                    healthMedicine.add("ClosedSpace");
                    humanities.add("ClosedSpace");
                    socialScience.add("ClosedSpace");
                    artisticField.add("ClosedSpace");
                }
                else
                {
                    healthMedicine.remove("ClosedSpace");
                    humanities.remove("ClosedSpace");
                    socialScience.remove("ClosedSpace");
                    artisticField.remove("ClosedSpace");

                }
                break;
            case R.id.cbOpenSpace:
                if (checked) {
                    biotechnicalScience.add("OpenSpace");
                }
                else
                {
                    biotechnicalScience.remove("OpenSpace");
                }
                break;
            case R.id.cbBothClosedOpen:
                if (checked) {
                    technicalScience.add("BothClosedOpen");
                    naturalScience.add("BothClosedOpen");
                }
                else
                {
                    technicalScience.remove("BothClosedOpen");
                    naturalScience.remove("BothClosedOpen");
                }
                break;
            case R.id.cbTravel:
                if (checked) {
                    socialScience.add("Travel");
                }
                else
                {
                    socialScience.remove("Travel");
                }
                break;
            case R.id.cbHome:
                if (checked) {
                    technicalScience.add("Home");
                    artisticField.add("Home");
                }
                else
                {
                    technicalScience.remove("Home");
                    artisticField.remove("Home");
                }
                break;

        }
    }
    //endregion
//region Results
    @OnClick(R.id.bShowResult)
    public void setbShowResult()
    {
        int result = calculateInterest();
        String field = "field";
        if(naturalScience.size()==result)
            field = "Prirodne znanosti";
        if(socialScience.size()==result)
            field = "Društvene znanosti";
        if(technicalScience.size()==result)
            field = "Tehničke znanosti";
        if(artisticField.size()==result)
            field = "Umjetničko područje";
        if(biotechnicalScience.size()==result)
            field = "Biotehničke znanosti";
        if(healthMedicine.size()==result)
            field = "Biomedicina i zdravstvo";
        if(humanities.size()==result)
            field = "Humanističke znanosti";

        Intent intent = new Intent();
        intent.setClass(this, CollegeTestResultActivity.class);
        intent.putExtra(CollegeTestResultActivity.KEY_FIELD, field);
        this.startActivity(intent);
    }

    public int calculateInterest()
    {
        int [] fields = new int[] {naturalScience.size(), socialScience.size(), technicalScience.size(), artisticField.size(), biotechnicalScience.size(), healthMedicine.size(), humanities.size()};
        int larger;
        for(int i = 0; i<fields.length; i++)
        {
            larger = 0;
            for(int j = i+1; j<fields.length; j++)
            {
                if(fields[i]<fields[j])
                {
                    larger = fields[i];
                    fields[i] = fields[j];
                    fields[j] = larger;
                }
            }
        }
        return fields[0];
    }
    //endregion
}