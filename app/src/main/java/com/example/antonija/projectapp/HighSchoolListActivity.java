package com.example.antonija.projectapp;

import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class HighSchoolListActivity extends AppCompatActivity {
    @BindView(R.id.bHighSchoolChoice)
    ImageButton bHighSchoolChoice;
    @BindView(R.id.lvHighSchoolList)
    ListView lvHighSchoolList;

    private DataBaseHelper dbHelper;
    HighschoolAdapter HSadapter;
    ArrayList<Highschool> HSArrayList = new ArrayList<Highschool>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_school_list);
        ButterKnife.bind(this);
        Picasso.with(getApplicationContext()).load(R.drawable.searchiconwhite32px).fit().centerInside().into(bHighSchoolChoice);
        this.loadDatabase();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //region ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_start:
                goToStart();
                return true;
            case R.id.menu_help:
                goToHelp();
                return true;
            case R.id.menu_test:
                goToTest();
                return true;
            case R.id.menu_about:
                goToAbout();
                return true;
            case R.id.menu_camera:
                goToCamera();}
        return false;
    }

    private void goToStart() {
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        this.startActivity(intent);
    }

    private void goToAbout() {
        Intent intent = new Intent();
        intent.setClass(this, AboutActivity.class);
        this.startActivity(intent);
    }

    private void goToTest() {
        Intent intent = new Intent();
        intent.setClass(this, TestHSActivity.class);
        this.startActivity(intent);
    }

    private void goToHelp() {
        Intent intent = new Intent();
        intent.setClass(this, HelpActivity.class);
        this.startActivity(intent);
    }
    public void goToCamera()
    {
        Intent intent = new Intent();
        intent.setClass(this, CameraActivity.class);
        this.startActivity(intent);
    }
    //endregion

    public void loadDatabase()
    {
        dbHelper=new DataBaseHelper(this);
        try
        {
            dbHelper.checkAndCopyDatabase();
            dbHelper.openDatabase();
        }
        catch (SQLException e)
        {
        }
        try
        {
            Cursor cursor = dbHelper.QueryData("select * from HighSchoolTable");
            if (cursor!=null)
            {
                if(cursor.moveToFirst())
                {
                    do
                    {
                        Highschool highschool = new Highschool();
                        highschool.set_id(cursor.getString(0));
                        highschool.set_name(cursor.getString(1));
                        highschool.set_type(cursor.getString(2));
                        highschool.set_program(cursor.getString(3));
                        highschool.set_location(cursor.getString(4));
                        highschool.set_website(cursor.getString(5));
                        highschool.set_latlong(cursor.getString(7));
                        HSArrayList.add(highschool);
                    }
                    while (cursor.moveToNext());
                }
            }
        }
        catch (SQLException e)
        {
        }
        HSadapter = new HighschoolAdapter(this, R.layout.highschool_list_item, HSArrayList);
        lvHighSchoolList.setAdapter(HSadapter);
        HSadapter.notifyDataSetChanged();
    }

    @OnClick(R.id.bHighSchoolChoice)
    public void goToSearch()
    {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), SearchHSActivity.class);
        this.startActivity(intent);
    }
    @OnItemClick(R.id.lvHighSchoolList)
    public void showDetails(int position)
    {
        Highschool selectedHighSchool = (Highschool) lvHighSchoolList.getItemAtPosition(position);
        String name = selectedHighSchool.get_name();
        String program = selectedHighSchool.get_program();
        String type = selectedHighSchool.get_type();
        String location = selectedHighSchool.get_location();
        String website = selectedHighSchool.get_website();
        String latlong = selectedHighSchool.get_latlong();
        Intent intent = new Intent();
        intent.setClass(this, DetailsHSActivity.class);
        intent.putExtra(DetailsHSActivity.KEY_NAME, name);
        intent.putExtra(DetailsHSActivity.KEY_PROGRAM, program);
        intent.putExtra(DetailsHSActivity.KEY_TYPE, type);
        intent.putExtra(DetailsHSActivity.KEY_LOCATION, location);
        intent.putExtra(DetailsHSActivity.KEY_WEBSITE, website);
        intent.putExtra(DetailsHSActivity.KEY_LATLONG, latlong);

        this.startActivity(intent);
    }
}
