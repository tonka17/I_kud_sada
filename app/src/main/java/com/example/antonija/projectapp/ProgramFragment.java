package com.example.antonija.projectapp;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemSelected;


public class ProgramFragment extends Fragment {

    @BindView(R.id.etEnterHSProgram)
    EditText etEnterHSProgram;
    @BindView(R.id.bHSProgram)
    Button bHSProgram;
    @BindView(R.id.lvHighSchoolList)
    ListView lvHighSchoolList;

    public static final String TITLE = "Obrazovni program";
    String selected = "";

    private DataBaseHelper dbHelper;
    HighschoolAdapter HSadapter;
    ArrayList<Highschool> HSArrayList = new ArrayList<Highschool>();

    public ProgramFragment() {
    }

    @Override
    public void onCreate
            (@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_program, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @OnClick(R.id.bHSProgram)
    public void filterProgram() {
        String entry = etEnterHSProgram.getText().toString();
        HSArrayList.clear();
        dbHelper = new DataBaseHelper(getContext());
        try {
            dbHelper.checkAndCopyDatabase();
            dbHelper.openDatabase();
        } catch (SQLException e) {
        }
        try {
            Cursor cursor = dbHelper.QueryData("select * from HighSchoolTable where lower(program) like '" + entry + "%'");
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        Highschool highschool = new Highschool();
                        highschool.set_id(cursor.getString(0));
                        highschool.set_name(cursor.getString(1));
                        highschool.set_type(cursor.getString(2));
                        highschool.set_program(cursor.getString(3));
                        highschool.set_location(cursor.getString(4));
                        highschool.set_website(cursor.getString(5));
                        highschool.set_latlong(cursor.getString(7));
                        HSArrayList.add(highschool);
                    }
                    while (cursor.moveToNext());
                }
            }
        } catch (SQLException e) {
        }
        HSadapter = new HighschoolAdapter((Activity) getContext(), R.layout.highschool_list_item, HSArrayList);
        lvHighSchoolList.setAdapter(HSadapter);
        HSadapter.notifyDataSetChanged();
    }
    @OnItemClick(R.id.lvHighSchoolList)
    public void showDetails(int position)
    {
        Highschool selectedHighSchool = (Highschool) lvHighSchoolList.getItemAtPosition(position);
        String name = selectedHighSchool.get_name();
        String program = selectedHighSchool.get_program();
        String type = selectedHighSchool.get_type();
        String location = selectedHighSchool.get_location();
        String website = selectedHighSchool.get_website();
        String latlong = selectedHighSchool.get_latlong();
        Intent intent = new Intent();
        intent.setClass(getContext(), DetailsHSActivity.class);
        intent.putExtra(DetailsHSActivity.KEY_NAME, name);
        intent.putExtra(DetailsHSActivity.KEY_PROGRAM, program);
        intent.putExtra(DetailsHSActivity.KEY_TYPE, type);
        intent.putExtra(DetailsHSActivity.KEY_LOCATION, location);
        intent.putExtra(DetailsHSActivity.KEY_WEBSITE, website);
        intent.putExtra(DetailsHSActivity.KEY_LATLONG, latlong);
        this.startActivity(intent);
    }
}
